compile_flags = -g3 -O0 -Wall -Werror

.PHONY: clean open

./build/prog: ./Lib/main.o ./Lib/Ins.o ./Lib/Heap.o ./Lib/quSort.o ./src/Sort.h build
	gcc -Wall -o ./build/prog ./Lib/main.o ./Lib/Ins.o ./Lib/Heap.o ./Lib/quSort.o ./src/Sort.h -lm

./Lib/Ins.o: ./src/Ins.c Lib
	gcc $(compile_flags) -o ./Lib/Ins.o -c ./src/Ins.c -lm

./Lib/Heap.o: ./src/Heap.c Lib
	gcc $(compile_flags) -o ./Lib/Heap.o -c ./src/Heap.c -lm

./Lib/main.o: ./src/main.c Lib
	gcc $(compile_flags) -o ./Lib/main.o -c ./src/main.c -lm

./Lib/quSort.o: ./src/quSort.c Lib
	gcc $(compile_flags) -o ./Lib/quSort.o -c ./src/quSort.c -lm
	
build:
	mkdir build 

Lib:
	mkdir Lib

clean :
	rm -rf build

open :
	./build/prog