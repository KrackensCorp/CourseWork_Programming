#include "Sort.h"

void InsertionSort(int *mas, int size) {
    int newElement;
 
	for (int j = 0; j < size; j++) {
		while (j > 0 && mas[j] < mas[j - 1]) {
			newElement = mas[j];
		    mas[j] = mas[j - 1];
		    mas[j - 1] = newElement;
			j--;
		}
	}
}
