#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Sort.h"

int compare_int(const void *elem1, const void *elem2);

int compare_char(const void *elem1, const void *elem2);

int main() {
	int size; 
	int flag = 0;
	int p;
	int size_1;
	int input; 
	int *mas = NULL;
	int pokaz = 5;

	printf("Я могу показать вам...\n");
	printf("1)Сортировка вставками\n");
	printf("2)Пирамидальная сортировка\n");		
	printf("3)qsort\n");
	printf("4)Выход\n");
	printf("Размер массива: ");
	scanf("%d", &size);
	
	if(size <= 0) {
		printf("Отрицательный размер массива.. Мб еще на ноль делить будем?)\n"); //ПРОВЕРКА ВВЕДЕННЫХ ДАННЫХ(ДЛИННА МАССИВА)
		return 0;
	}

	mas = (int*) malloc(size * sizeof(size));
	srand(time(NULL));	
	
	for(int i = 0; i < size; i++) {
		mas[i] = rand()%100;
	}
	
	printf("Хотите массив покажу? (1 - да, 0 - нет) \n");
    scanf("%d", &pokaz);
	printf("\n");
	
	if(pokaz == 1) {
		for(int i = 0; i < size; i++) {
			printf("%d\t", mas[i]);
		}
	} else {
		printf("Жаль...");
	}
	printf("\n");	
	

	while (flag == 0) {
		printf("Что будем делать?: ");
		scanf("%d", &input);

		switch(input) {

			case 1: // InsertionSort

				printf("Correct array(InsertionSort):\n");

				srand(time(NULL));
				double start = clock();
				InsertionSort(mas, size);
				double end = clock();

				for(int i = 0; i < size; i++) {
					printf("mas[%d]=%d\n", i, mas[i]);
				}

				printf("Затрачено всего %f секунд\n", difftime(end, start)/1000000);
				printf("\n");
			break;

			case 2: // HeapSort
				printf("Correct array(HeapSort):\n");

				double start_1 = clock();
				HeapSort(mas, size); 
				double end_1 = clock();
				
				for(int i = 0; i < size; i++) {
					printf("mas[%d]=%d\n", i, mas[i]);
				}

				printf("Затрачено всего %f секунд\n", difftime(end_1, start_1)/1000000);
				printf("\n");	
			break;

			case 3: // quSort
				printf("Тип? (1 - int,2 - char)\n");
				scanf("%d", &p);

				if(p == 1) {
					printf("Введите новый размер массива: \n");
					scanf("%d", &size_1);
					
					int array[size_1];
					
					for(int j = 0; j < size_1; j++) {
						scanf("%d", &array[j]);
					}
					q_sort(array, size_1, sizeof(int), compare_int);
					for(int j = 0; j < size_1; j++) {
					 	printf("mas[%d] = %d\n", j, array[j]);
					}
				} else if(p == 2) {
					printf("Введите новый размер массива: ");
					scanf("%d", &size_1);
					
					size_1 += 1;
					char array_1[size_1];

					for(int j = 0; j < size_1; j++) {
						scanf("%c", &array_1[j]);
					}
					q_sort(array_1, size_1, sizeof(char), compare_char);
					for(int j = 1; j < size_1; j++) {
						printf("mas[%d]=%c\n", j, array_1[j]);
					}
					printf("\nmas[0] = %c*mas[0] - символ конца строки*\n\n", array_1[0]);				
				} else { 
					if(p != 2 && p != 1) {
                		printf("Ошибка (все нормально, все мы ошибаемся, не волнуйтесь, начните заного)\n");
					}
				}
			break;
			
			case 4:
				flag = 1;
			break;

		    default:
			    printf("Ширяев Андрей ИС-742\n");
		    break;	
		}
	}

	printf("Выход\n");
	
	free(mas); //ОСВОБОЖДЕНИЕ ВЫДЕЛЕННОЙ ПОД МАССИВ ПАМЯТИ
	mas = NULL;

	printf("<3\n");
	
	return 0;
}