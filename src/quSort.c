#include "Sort.h"
// Массив сортируется по возрастанию таким образом, что наименьший 
//          адрес соответствует наи­меньшему элементу.
int compare_int(const void *elem1, const void *elem2) {
	return ((*(int *)elem1) - (*(int *)elem2));
	}

int compare_char(const void *elem1, const void *elem2) {
	return ((*(char *)elem1) - (*(char *)elem2));
	}

void q_sort(void *mas, int size, int width, int(*cmp)(const void *elem1, const void *elem2)) {
	int i = 0;
	int j = 0;
	int k = 0;
	char tmp;

	for (i = 0; i < size - 1; i++) {
		for (j = 0; j < (size - i - 1); j++) {
			if (cmp((char *)mas + j * width, (char *)mas + (j+1) * width) > 0) {
				for (k = 0; k < width; k++) {
					tmp = *((char*)mas + k + j * width);
					*((char*)mas + k + j * width) = *((char*)mas + k + (j + 1) * width);
					*((char*)mas + k + (j + 1) * width) = tmp;
				}			
			}
		}
	}
}
