#include "Sort.h"

void DownHeap(int *mas, int index, int size, int Current) {
    //Функция пробегает по пирамиде восстанавливая ее
    //Также используется для изначального создания пирамиды
    //Использование: Передать номер следующего элемента в index
    //Процедура пробежит по всем потомкам и найдет нужное место для следующего элемента
    int Child;
    while (index < (size / 2)) {
        Child = (index + 1) * 2 - 1;
        if ((Child < size - 1) && (mas[Child] < mas[Child + 1])) Child++;
        if (Current >= mas[Child]) break;
        mas[index] = mas[Child];
        index = Child;
    }
    mas[index] = Current;
}
 
void HeapSort(int *mas, int size) {
//Основная функция
    int i;
    int Current;
    //Собираем пирамиду
    for (i = (size / 2) - 1; i > 0; i--) DownHeap(mas, i, size, mas[i]);
  //Пирамида собрана. Теперь сортируем
    for (i = size - 1; i > 0; i--) {
        Current = mas[i]; //перемещаем верхушку в начало отсортированного списка
        mas[i] = mas[0];
        DownHeap(mas, 0, i, Current); //находим нужное место в пирамиде для нового элемента
    }
}