#ifndef SORT_H
#define SORT_H

#include <stdio.h>
#include <stdlib.h>

void HeapSort(int *mas, int size);
void InsertionSort(int *mas, int size);
void q_sort(void *array, int num, int width, int(*cmp)(const void *elem1, const void *elem2));

#endif